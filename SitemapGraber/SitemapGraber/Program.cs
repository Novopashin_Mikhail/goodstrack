﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SitemapStandart;
using System.Xml;
using System.Xml.Serialization;
using AmzLinkParser;

namespace SitemapGraber
{
    class Program
    {
        static void Main(string[] args)
        {
            string sUrl=@"https://10beasts.com/sitemap.xml";
           
            // Запустим грабер ссылок из XML карты сайта и соберем из карты и вложенных в нее дочерних карт ссылки на страницы
            SitemapGrab Graber = new SitemapGrab(sUrl);
            Graber.DoGrab();

            Console.WriteLine("В карте сайта найдены следующие страницы:");
            Graber.ShowLinks();
            
            // парсим ссылки из статей
            string LinkForCheck = Graber.resultUrlSet.Url[10].Loc; // возьмем 10-ую для примера, я просто точно знаю что там есть.
         
            AmzLinkParser.AmzLinkParser amParser = new AmzLinkParser.AmzLinkParser(LinkForCheck);         
            Console.WriteLine("парсим страничку: " + LinkForCheck);
            amParser.Parse();
            if (amParser.lSResult.Count > 0)
            {
                Console.WriteLine("Найдены ссылки:");
                foreach (string temLink in amParser.lSResult)
                {
                    Console.WriteLine(temLink);
                }
            }
            else
                Console.WriteLine("Ссылок не найдено!");
          
            Console.WriteLine("Работа завершена, для продолжения нажмите Enter");
            Console.ReadLine();
        }
    }
}
