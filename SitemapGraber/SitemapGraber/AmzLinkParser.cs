﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace AmzLinkParser
{
    class AmzLinkParser
    {
        private string fUrl;
        public List<string> lSResult;
        public AmzLinkParser(string pUrl)
        {
            fUrl = pUrl;
            lSResult = new List<string>();
        }
        public void Parse()
        {
            HtmlAttribute haHref;
            HtmlNode hnRoot;
            HtmlNodeCollection hcSearchResults;           
            string sLink;
           
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument htmlDoc = web.Load(fUrl);
            
            hnRoot = htmlDoc.DocumentNode;
            hcSearchResults = hnRoot.SelectNodes(@"//a");

            foreach (HtmlNode hnLink in hcSearchResults)
            {
                haHref = hnLink.Attributes["href"];
                if (haHref != null)
                {
                    sLink = haHref.Value;
                    if (sLink.isAmzLink())
                    {
                        lSResult.Add(sLink);
                    }
                }
            }
            lSResult = lSResult.Distinct().ToList();
        }
    }
}
