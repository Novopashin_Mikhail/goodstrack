﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace AmzLinkParser
{
   static  class AmzStringExtension
   {
        public static Boolean isAmzLink(this string psLink)
        {
            return  ((Regex.IsMatch(psLink, @"amzn\.to.*")) ||  (Regex.IsMatch(psLink, @"amazon\.com.*"))) ;        
        }
    }
}
